#include <iostream>

struct INPUT_PARAM
{
    std::string model_path;
    std::string model_name;
    std::string version;
};
class AiCore{
    public:
        AiCore();
        ~AiCore();
        bool Init(INPUT_PARAM);
        bool Infer(void *);
}